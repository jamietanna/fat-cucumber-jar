package me.jvt.hacking;

import org.junit.ComparisonFailure;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.extractProperty;

public class StepsTest {
    private Steps sut = new Steps();

    @Test
    public void createANewListCreatesANewInstanceOfAnEmptyList () {
        List<String> oldList = sut.getList();
        sut.createANewList();
        List<String> newList = sut.getList();

        assertThat(oldList).isNotSameAs(newList);
        assertThat(newList).isEmpty();
    }

    @Test
    public void appendNewItemAppendsNewItemToList() {
        List<String> list = sut.getList();
        sut.appendItemToList();

        assertThat(list.size()).isEqualTo(1);
    }

    @Test
    public void appendNewItemThreeTimesAppendsThreeItemsToList() {
        List<String> list = sut.getList();
        sut.appendItemToList();
        sut.appendItemToList();
        sut.appendItemToList();

        assertThat(list.size()).isEqualTo(3);
    }

    @Test
    public void theListHasItemsInItDoesNotFailWhenExpectedItemsMatchActual() {
        List<String> injectedList = new ArrayList<String>();
        injectedList.add("Wibble");
        injectedList.add("BAR");
        injectedList.add("foo");
        sut = new Steps(injectedList);

        sut.theListHasItemsInIt(3);

        // no exception
    }

    @Test
    public void theListHasItemsInItRaisesWhenExpectedItemsDoNotMatchActual() {
        List<String> injectedList = new ArrayList<String>();
        injectedList.add("Wibble");
        injectedList.add("BAR");
        injectedList.add("foo");
        sut = new Steps(injectedList);

        ComparisonFailure actualError = null;
        try {
            sut.theListHasItemsInIt(5);
        } catch(ComparisonFailure e) {
            actualError = e;
        }

        assertThat(actualError.getExpected()).isEqualTo("5");
        assertThat(actualError.getActual()).isEqualTo("3");
    }
}
