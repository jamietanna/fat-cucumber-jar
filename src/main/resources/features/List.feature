Feature: List

  Scenario: When I append to a list, it appends
    Given I create a new List
    When I append a new item
    Then the list has 1 item in it

  Scenario: When I append to a list, it appends
    Given I create a new List
    When I append a new item
    And I append a new item
    Then the list has 2 items in it
