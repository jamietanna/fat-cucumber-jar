package me.jvt.hacking;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class Steps {
    private List<String> list;

    public Steps() {
        list = new ArrayList<String>();
    }

    public Steps(List<String> theList) {
        list = theList;
    }

    @When("I append a new item")
    public void appendItemToList() {
        list.add("wibble");
    }

    public List<String> getList() {
        return list;
    }

    @Given("I create a new List")
    public void createANewList() {
        list = new ArrayList<String>();
    }

    @Then("^the list has (\\d+) items? in it$")
    public void theListHasItemsInIt(int items) {
        assertThat(list.size()).isEqualTo(items);
    }
}
