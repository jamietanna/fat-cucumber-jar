package me.jvt.hacking;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.JUnitCore;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = {"pretty", "de.monochromata.cucumber.report.PrettyReports:target/pretty-cucumber", "html:target/cucumber-html", "json:target/report.json"}, features = {"classpath:features"})
public class RunCukes {
   public static void main(String[] args) {
     JUnitCore.main(RunCukes.class.getName());
   }
}
